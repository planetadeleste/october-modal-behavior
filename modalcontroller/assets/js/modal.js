/**
 * Created by alvaro on 26/10/15.
 */
+function ($) {
    "use strict";

    var ManageModal = function () {

        this.clickRecord = function (recordId) {
            var newPopup = $('<a />');

            newPopup.popup({
                handler: 'onUpdateForm',
                extraData: {
                    'record_id': recordId,
                }
            })
        };

        this.createRecord = function () {
            var newPopup = $('<a />');
            newPopup.popup({ handler: 'onCreateForm' });

            return false;
        }

    };

    $.manageModal = new ManageModal;

}(window.jQuery);